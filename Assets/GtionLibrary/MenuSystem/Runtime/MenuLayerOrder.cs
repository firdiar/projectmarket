﻿namespace Gtion.Plugin.MenuSystem
{
    public enum MenuLayerOrder
    {
        BACKGROUND,
        MAIN,
        POPUP,
        NOTIFICATION,
        VFX,
        FRONT,
        VFX_FRONT,
        NONE
    }

    public enum MenuCanvasOrder
    {
        OVERLAY,
        CAMERA
    }
}
