﻿using NaughtyAttributes;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using System.IO;


namespace Gtion.Plugin.MenuSystem.Configuration
{
    using Gtion.Plugin.MenuSystem.Core;

    public class MenuSystemConfiguration : ScriptableObject
    {
        const string configPath = "Assets/Resources/MenuSystem/MenuSystemConf.asset";
        const string configResourcePath = "MenuSystem/MenuSystemConf";

        [SerializeField] [ReorderableList]
        private List<MenuConfiguration> config = new List<MenuConfiguration>();

        public void RegisterConfig(MenuConfiguration newConfig)
        {
            for(int i = 0; i  < config.Count; i++)
            {
                if (config[i].ClassName == newConfig.ClassName)
                {
                    config[i] = newConfig;
                    return;
                }
            }
            config.Add(newConfig);
        }

        public void UnregistConfig(MenuConfiguration newConfig)
        {
            for (int i = 0; i < config.Count; i++)
            {
                if (config[i].ClassName == newConfig.ClassName)
                {
                    config.RemoveAt(i);
                    Debug.Log("Unregister Success!");
                    return;
                }
            }
            Debug.Log($"Can't Found Class `{newConfig.ClassName}` in MenuSystemConfig");
        }

        public MenuConfiguration GetConfig(string className) 
        {
            foreach (MenuConfiguration conf in config)
            {
                if (conf.ClassName == className)
                {
                    return conf;
                }
            }
            Debug.LogError($"MENU_MANAGER : Default configuration for '{className}' not found");
            return null;
        }

        public MenuConfiguration GetConfigDuplicate(string className)
        {
            MenuConfiguration menuConf = GetConfig(className);
            if (menuConf != null)
            {
                return menuConf.CreateDuplicate();
            }
            return null;
        }

        public static MenuSystemConfiguration LoadConfig() 
        {
            MenuSystemConfiguration menuConfig = Resources.Load<MenuSystemConfiguration>(configResourcePath);

            if (menuConfig == null)
                Debug.LogError($"Configuration file not exist, please put conf file here: '{configPath}'");

            return menuConfig;
        }


#if UNITY_EDITOR
        [MenuItem("Gtion/MenuSystem/Create-ConfigFile")]
        public static void CreateConfigAsset()
        {
            bool canCreate = true;
            MenuSystemConfiguration menuConfig = LoadConfig();
            if (menuConfig != null)
            {
                canCreate = EditorUtility.DisplayDialog("Warning!","Config-file is already exist, are you sure want to replace?", "Replace", "No");
            }

            if (canCreate)
            {
                menuConfig = ScriptableObject.CreateInstance<MenuSystemConfiguration>();
                string directory = Path.GetDirectoryName(configPath);
                if (!Directory.Exists(directory)) 
                {
                    Directory.CreateDirectory(directory);
                }
                AssetDatabase.Refresh();

                AssetDatabase.CreateAsset(menuConfig, configPath);
                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();
            }

            EditorUtility.FocusProjectWindow();
            Selection.activeObject = menuConfig;
        }

        [MenuItem("Gtion/MenuSystem/Goto Configuration")]
        public static void GotoConfigAsset()
        {
            MenuSystemConfiguration menuConfig = LoadConfig();
            if (menuConfig == null)
            {
                EditorUtility.DisplayDialog("Warning!", "Config-file is not created yet.", "Ok");
            }
            else 
            {
                EditorUtility.FocusProjectWindow();
                Selection.activeObject = menuConfig;
            }
        }

        [MenuItem("Gtion/MenuSystem/Validate-ConfigFile")]
        [Button]
        public static void ValidateConfigAsset()
        {
            MenuSystemConfiguration menuConfig = LoadConfig();
            if (menuConfig != null)
            {
                foreach (MenuConfiguration conf in menuConfig.config)
                {
                    GameObject temp = conf.GetGameObjectPrefab();

                    if (temp == null)
                    {
                        Debug.LogWarning($"Check Configuration '{conf.ClassName}' : Not Found. \nRef Type: {conf.ReferenceType}");
                    }
                    else
                    {
                        var controller = temp.GetComponent<GMenuBaseController>();
                        Debug.Log($"Check Configuration '{conf.ClassName}' : Ok ({controller.GetType().Name})");
                        controller.MenuConfig = conf;
                        EditorUtility.SetDirty(temp);
                    }
                }
                AssetDatabase.SaveAssets();
            }
        }

        [Button]
        protected void Clean()
        {
            MenuSystemConfiguration menuConfig = LoadConfig();
            if (menuConfig != null)
            {
                var conf = menuConfig.config;
                for (int i = conf.Count - 1; i >= 0; i--)
                {
                    if (conf[i].IsDedicated)
                    {
                        Debug.Log("Removing Dedicated `" + conf[i].ClassName + "`");
                        conf.RemoveAt(i);
                    }
                    else
                    {
                        GameObject temp = conf[i].GetGameObjectPrefab();

                        if (temp == null)
                        {
                            Debug.Log("Removing Unused `" + conf[i].ClassName + "`");
                            conf.RemoveAt(i);
                        }
                    }
                }
            }

            Save();
        }

        /// <summary>
        /// Download changes from targetObject
        /// </summary>
        [Button]
        protected void DownloadChanges()
        {
            MenuSystemConfiguration menuConfig = LoadConfig();
            if (menuConfig != null)
            {
                var conf = menuConfig.config;
                for (int i = conf.Count - 1; i >= 0; i--)
                {
                    GameObject temp = conf[i].GetGameObjectPrefab();

                    if (temp == null)
                    {
                        Debug.Log("Unused `" + conf[i].ClassName + "` at "+i);
                        conf.RemoveAt(i);
                    }
                    else
                    {
                        menuConfig.config[i] = temp.GetComponent<GMenuBaseController>().MenuConfig;
                    }
                }
            }

            Save();
        }

        /// <summary>
        /// Share changes to targetObject
        /// </summary>
        [Button]       
        protected void UploadChanges()
        {
            MenuSystemConfiguration menuConfig = LoadConfig();
            if (menuConfig != null)
            {
                var conf = menuConfig.config;
                for (int i = conf.Count - 1; i >= 0; i--)
                {
                    GameObject temp = conf[i].GetGameObjectPrefab();

                    if (temp == null)
                    {
                        Debug.Log("Unused `" + conf[i].ClassName + "` at " + i);
                        conf.RemoveAt(i);
                    }
                    else
                    {
                        temp.GetComponent<GMenuBaseController>().MenuConfig = menuConfig.config[i];
                        EditorUtility.SetDirty(temp);
                    }
                }
            }

            Save();
        }

        public void Save() 
        {
            MenuSystemConfiguration menuConfig = LoadConfig();
            if (menuConfig == null)
            {
                EditorUtility.DisplayDialog("Warning!", "Config-file is not created yet.", "Ok");
                return;
            }

            //AutoSave
            AssetDatabase.Refresh();
            EditorUtility.SetDirty(menuConfig);
            AssetDatabase.SaveAssets();
        }
#endif
    }
}
