﻿
using NaughtyAttributes;
using UnityEngine.AddressableAssets;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.AddressableAssets;
using UnityEditor.AddressableAssets.Settings;
#endif

namespace Gtion.Plugin.MenuSystem.Configuration
{
    using Gtion.Plugin.MenuSystem.Core;
    using System.Threading.Tasks;

    public enum ReferenceType
    { 
        Dedicated = 0,
        Path = 1,
        Prefab = 2,
        Addressable = 3
    }

    [System.Serializable]
    public class MenuConfiguration
    {
        [Header("Setting")]
        [Tooltip("IdentifierClass")]
        [SerializeField]
        [ReadOnly]
        [AllowNesting]
        private string className = "MenuBase";
        [Tooltip("IdentifierClass")]
        [SerializeField]
        [ReadOnly]
        [AllowNesting]
        private string guid = string.Empty;
        [Tooltip("Closing Some Unused Tab On Dedicated Controller")]
        [SerializeField]
        protected bool isDedicated;

        [Header("Placement")]
        [Space(10)]
        [Tooltip("Where to instantiate canvas")]
        [SerializeField]
        [HideIf(nameof(isDedicated))]
        [AllowNesting]
        private MenuCanvasOrder canvas; // Overlay
        [Tooltip("Order to show in UI Layer")]
        [SerializeField]
        [HideIf(nameof(isDedicated))]
        [AllowNesting]
        private MenuLayerOrder layerOrder = MenuLayerOrder.MAIN;
        [Tooltip("Add backpress order, will execute this first instead another menu")]
        [SerializeField]
        [AllowNesting]
        private bool blockBackPress = true;
        [Tooltip("Will run methode `Hide` on backpress clicked")]
        [SerializeField]
        [HideIf(nameof(isDedicated))]
        [AllowNesting]
        private bool hideOnBackPress = true;

        [Header("Reference")]
        [Space(10)]
        [SerializeField]
        [ReadOnly]
        ReferenceType referenceType;

        [Tooltip("Path to gameobject prefabs in 'Resources' Folder, [Slow, Dynamic-Memory usage]")]
        [SerializeField]
        [ReadOnly]
        [HideIf(nameof(isDedicated))]
        [AllowNesting]
        private string pathGameObject = "Prefabs/Menu/Base";
        [Tooltip("Path to gameobject prefabs, [Fast, Static-Memory usage]")]
        [SerializeField]
        [ReadOnly]
        [HideIf(nameof(isDedicated))]
        [AllowNesting]
        private GameObject prefabsGameObject;
        [Tooltip("Asset Reference to GameObject, DONT CHANGE IT MANUALLY, [Slow, Dynamic-Memory usage]")]
        [SerializeField]
        private AssetReferenceWrapper<GameObject> refGameObject;

        /// <summary>
        /// IdentifierClass
        /// </summary>
        public string ClassName => className;
        /// <summary>
        /// IdentifierClass
        /// </summary>
        public string GUID => guid;
        public bool IsDedicated => isDedicated;
        public ReferenceType ReferenceType => referenceType;
        /*
        /// <summary>
        /// Path to gameobject prefabs in 'Resources' Folder
        /// </summary>
        public string GameObjectPath => pathGameObject;
        /// <summary>
        /// Path to gameobject prefabs []
        /// </summary>
        public GameObject GameObjectPrefabs => prefabsGameObject;
        /// <summary>
        /// Asset Reference to GameObject
        /// </summary>
        public AssetReferenceWrapper<GameObject> GameObjectRef => refGameObject;
        */
        /// <summary>
        /// Where to instantiate canvas
        /// </summary>
        public MenuCanvasOrder Canvas => canvas;
        /// <summary>
        /// Order to show in UI Layer
        /// </summary>
        public MenuLayerOrder LayerOrder => layerOrder;
        /// <summary>
        /// Add backpress order, will execute this first instead another menu
        /// </summary>
        public bool BlockBackPress => blockBackPress;
        /// <summary>
        /// Will run methode `Hide` on backpress clicked
        /// </summary>
        public bool HideOnBackPress => hideOnBackPress;

        public MenuConfiguration CreateDuplicate()
        {
            MenuConfiguration newMenuConfig = new MenuConfiguration();
            newMenuConfig.className = className;
            newMenuConfig.pathGameObject = pathGameObject;
            newMenuConfig.prefabsGameObject = prefabsGameObject;
            newMenuConfig.layerOrder = layerOrder;
            newMenuConfig.blockBackPress = blockBackPress;
            newMenuConfig.hideOnBackPress = hideOnBackPress;

            return newMenuConfig;
        }

        public GameObject GetGameObjectPrefab() 
        {
            switch (ReferenceType)
            {
                case ReferenceType.Path:
                    return Resources.Load<GameObject>(pathGameObject);
                case ReferenceType.Prefab:
                    return prefabsGameObject;
                case ReferenceType.Addressable:
                    return refGameObject.LoadAssetSync();
            }
            return null;
        }
#if UNITY_EDITOR
        /// <summary>
        /// Editor Only, to Setup object
        /// </summary>
        /// <param name="controller"></param>
        /// <param name="usePath"></param>
        public bool Setup(GMenuBaseController controller, ReferenceType refType = ReferenceType.Prefab)
        {
            AssetDatabase.Refresh();
            className = controller.GetType().Name;

            //var temp = Resources.FindObjectsOfTypeAll(controller.GetType());
            GameObject prefab = PrefabUtility.GetCorrespondingObjectFromOriginalSource(controller.gameObject);
            string path = AssetDatabase.GetAssetPath(prefab);
            if (string.IsNullOrEmpty(path))
            {
                var prefabMode = UnityEditor.SceneManagement.PrefabStageUtility.GetCurrentPrefabStage();
                if (prefabMode != null)
                {
                    var tempPath = prefabMode.assetPath;
                    var pathObj = AssetDatabase.LoadAssetAtPath(tempPath, controller.GetType());
                    if (pathObj != null)
                    {
                        Debug.Log("Using Alternative PrefabMode");
                        path = tempPath;
                        controller = (GMenuBaseController)pathObj;
                        prefab = controller.gameObject;
                    }
                }
            }

            
            if (!string.IsNullOrEmpty(path))
            {
                guid = AssetDatabase.AssetPathToGUID(path);
            }
            else
            {
                Debug.Log("Can't Find Asset Path");
                return false;
            }

            Debug.Log("Asset Path : " + path);
            Debug.Log("Asset Name : " + prefab.name);

            if (refType == ReferenceType.Path)
            {
                bool isInResources = path.Contains("Resources");

                if (isInResources)
                {
                    var dirs = path.Split('/');
                    string result = string.Empty;
                    bool isResourcesPassed = false;
                    for (int i = 0; i < dirs.Length - 1; i++)
                    {
                        if (!isResourcesPassed && dirs[i] != "Resources") continue;
                        if (!isResourcesPassed)
                        {
                            isResourcesPassed = true;
                            continue;
                        }

                        result += dirs[i] + "/";
                    }
                    result += dirs[dirs.Length - 1].Replace(".prefab", string.Empty);
                    pathGameObject = result;
                    refGameObject.SetEditorAsset(null);
                    prefabsGameObject = null;
                }
                else
                {
                    Debug.LogError("Unable to add as Path, Prefab have to be inside `Resources` folder");
                    return false;
                }
            }
            else if (refType == ReferenceType.Prefab)
            {
                pathGameObject = string.Empty;
                refGameObject.SetEditorAsset(null);
                prefabsGameObject = prefab;
            }
            else if (refType == ReferenceType.Addressable)
            {
                //Make a gameobject an addressable
                var settings = AddressableAssetSettingsDefaultObject.Settings;
                AddressableAssetGroup g = settings.DefaultGroup;

                if (settings.FindAssetEntry(guid) == null)
                {
                    if (EditorUtility.DisplayDialog("Attention", "This Prefab haven't registered as Addressable, do you want to mark it as addressable", "Yes", "Cancel Registration"))
                    {
                        //This is the function that actually makes the object addressable
                        var entry = settings.CreateOrMoveEntry(guid, g);
                        entry.labels.Add(prefab.name);
                        entry.address = prefab.name;

                        //You'll need these to run to save the changes!
                        settings.SetDirty(AddressableAssetSettings.ModificationEvent.EntryMoved, entry, true);
                        AssetDatabase.SaveAssets();
                        AssetDatabase.Refresh();
                    }
                    else
                    {
                        return false;
                    }
                }

                pathGameObject = string.Empty;
                prefabsGameObject = null;
                refGameObject.SetEditorAsset(prefab);
            }
            else
            {
                pathGameObject = string.Empty;
                prefabsGameObject = null;
                refGameObject.SetEditorAsset(null);
            }
            referenceType = refType;
            return true;
        }
#endif
    }
}