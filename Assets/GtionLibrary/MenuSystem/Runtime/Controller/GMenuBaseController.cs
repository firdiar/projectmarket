﻿using NaughtyAttributes;
using Gtion.Plugin.MenuSystem.Configuration;
using UnityEngine;

namespace Gtion.Plugin.MenuSystem
{
    using Gtion.Plugin.MenuSystem.Core;
    public interface IMenuModel
    { }

    public interface IMenuModel<T> : IMenuModel
    { }


    namespace Gtion.Plugin.MenuSystem.Core
    {
        /// <summary>
        /// 
        /// </summary>
        public abstract class GMenuBaseController : MonoBehaviour
        {
            [Header("Default Menu Base")]
            [ReadOnly]
            public string MenuKey = string.Empty;
            public MenuConfiguration MenuConfig;

            protected void Start()
            {
                if (MenuConfig.IsDedicated)
                {
                    MenuManager.RegisterDedicatedMenu(this);//dedicated menu exist in scene by default, so we have to trigger MenuManager to register menu in stack
                }
                Construct();
            }

            /// <summary>
            /// Called before show
            /// </summary>
            public abstract void Construct();

            /// <summary>
            /// Called before show
            /// </summary>
            public abstract void Init(IMenuModel passValue);

            /// <summary>
            /// Called 1 frame after instantiated
            /// </summary>
            public virtual void Show()
            {
                gameObject.SetActive(true);
            }

            /// <summary>
            /// API to hide this object without saving to pool
            /// </summary>
            public void Hide()
            {
                Hide(false);
            }

            /// <summary>
            /// API to hide this object
            /// </summary>
            /// <param name="saveToPool"></param>
            public virtual void Hide(bool saveToPool)
            {
                if (!saveToPool)
                {
                    MenuManager.Instance.HandleMenuDelete(MenuKey);
                    DestroyThisGameObject();
                }
                else
                {

                    if (MenuManager.Instance.HandleMenuPool(MenuKey))
                    {
                        DeactivateThisGameObject();
                    }
                    else
                    {
                        //forcely delete because limit exceed
                        MenuManager.Instance.HandleMenuDelete(MenuKey);
                        DestroyThisGameObject();
                    }
                }
            }

            /// <summary>
            /// Called when system want to destroy this object
            /// </summary>
            protected virtual void DestroyThisGameObject()
            {
                Destroy(gameObject);
            }

            /// <summary>
            /// Called When systen want to pool this object
            /// </summary>
            protected virtual void DeactivateThisGameObject()
            {
                gameObject.SetActive(false);
            }

            /// <summary>
            /// Called when backpress invoked according to backpress stack
            /// </summary>
            public virtual void OnBackPressed()
            {
                if (MenuConfig.HideOnBackPress) Hide();
            }

        }
    }
}
