﻿using Gtion.Plugin.MenuSystem.Configuration;
using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gtion.Plugin.MenuSystem
{
    using Gtion.Plugin.MenuSystem.Core;

    public abstract class GMenuController<T> : GMenuBaseController where T : IMenuModel
    {
        [SerializeField]
#if UNITY_EDITOR
        [FieldBtn(nameof(UnRegist) , nameof(RegisterConfigPath), nameof(RegisterConfigPrefab), nameof(RegisterConfigRefs))]
#endif
        bool isRegistered;

        [SerializeField]
        [HideField]
        [HorizontalLine]
        bool temp;

        private bool isConstructed;
        protected bool IsConstructed => isConstructed;

        private T model;
        protected T Model => model;

        public sealed override void Construct()
        {
            if (!IsConstructed)
            {
                OnConstruct();
            }
        }

        /// <summary>
        /// Called Once at First, Guaranteed to be called before Init
        /// </summary>
        public virtual void OnConstruct()
        {
            isConstructed = true;
        }

        public sealed override void Init(IMenuModel passValue)
        {
            model = (T)passValue;
            Construct();
            Init(model);
        }

        /// <summary>
        /// Initialize Controller with given data
        /// </summary>
        /// <param name="menuModel"></param>
        public virtual void Init(T menuModel)
        {             
        }

#if UNITY_EDITOR
        private void OnValidate()
        {
            if (MenuConfig.IsDedicated && isRegistered)
            {
                UnRegist();
                MenuConfig.Setup(this, ReferenceType.Dedicated);
            }
        }

        private void AddToConfig(ReferenceType refType)
        {
            if (MenuConfig.IsDedicated)
            {
                Debug.Log("Unable to Register Dedicated Menu");
                return;
            }

            var menuConfig = MenuSystemConfiguration.LoadConfig();
            if (menuConfig == null)
            {
                Debug.Log("Menu Config Not Yet Exist!");
                return;
            }

            bool isSuccess = MenuConfig.Setup(this, refType);
            if (isSuccess)
            {
                menuConfig.RegisterConfig(MenuConfig);
                Debug.Log("Registered!");
                isRegistered = true;
            }
            else
            {
                Debug.Log("Register Failed, try to refresh your unity");
            }

            //AutoSave
            menuConfig.Save();
        }

        protected void UnRegist()
        {
            var menuConfig = MenuSystemConfiguration.LoadConfig();
            if (menuConfig == null)
            {
                Debug.Log("Menu Config Not Yet Exist!");
                return;
            }

            menuConfig.UnregistConfig(MenuConfig);
            //AutoSave
            menuConfig.Save();
            isRegistered = false;
        }

        protected void RegisterConfigPath()
        {
            AddToConfig(ReferenceType.Path);
        }

        protected void RegisterConfigPrefab()
        {
            AddToConfig(ReferenceType.Prefab);
        }

        protected void RegisterConfigRefs()
        {
            AddToConfig(ReferenceType.Addressable);
        }
#endif
    }
}
