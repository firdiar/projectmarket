using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace Gtion.Plugin.MenuSystem
{
    [Serializable]
    public class AssetReferenceWrapper<TObject> : AssetReferenceT<TObject> where TObject : UnityEngine.Object
    {
        public AssetReferenceWrapper(string guid) : base(guid)
        { 
        }

        bool isLoading;
        public bool IsAssetLoaded => Asset != null;
        public bool IsLoading => isLoading;

        /// <summary>
        /// Load Asset Synchornously
        /// </summary>
        /// <returns></returns>
        public TObject LoadAssetSync() 
        {
#if UNITY_EDITOR
            if (!Application.isPlaying) return (TObject)editorAsset;
#endif

            if (IsAssetLoaded)
            {
                return (TObject)Asset;
            }

            if (isLoading)
            {
                Debug.LogError("Asynchronous Load is Running, you can't do both Sync & Async at one time.");
                return default;
            }

            isLoading = true;
            var opreation = base.LoadAssetAsync();            
            TObject result = opreation.WaitForCompletion();
            isLoading = false;
            return result;
        }

        /// <summary>
        /// Load Asset Asynchronousely
        /// </summary>
        /// <returns></returns>
        public new async Task<TObject> LoadAssetAsync()
        {
            return await LoadAssetAsync(25);
        }

        /// <summary>
        /// Load Asset Asynchronousely
        /// </summary>
        /// <returns></returns>
        public async Task<TObject> LoadAssetAsync(int delayCheck)
        {
#if UNITY_EDITOR
            if (!Application.isPlaying) return (TObject)editorAsset;
#endif

            while (isLoading) //wait if another process currently loading the asset
            {
                await Task.Delay(delayCheck);
            }

            if (IsAssetLoaded)
            {
                return (TObject)Asset;
            }

            isLoading = true;
            var result = await base.LoadAssetAsync().Task;
            isLoading = false;
            return result;
        }
    }
}
