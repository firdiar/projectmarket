﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Gtion.Plugin.MenuSystem.Configuration;
using UnityEngine.SceneManagement;
using System.Collections;

namespace Gtion.Plugin.MenuSystem
{
    using Gtion.Plugin.MenuSystem.Core;
    using NaughtyAttributes;

    public class MenuData
    {
        public Type TypeMenu;
        public MenuLayerOrder Layer = MenuLayerOrder.MAIN;
        public GMenuBaseController Menu;
        public BackpressAction Backpress;

        public bool IsActive => Menu.gameObject.activeInHierarchy;
    }

    public class BackpressAction
    {
        public Action callback;
    }

    [System.Serializable]
    public class MenuCanvasConfig
    {
        public Canvas Canvas;
        public Dictionary<MenuLayerOrder, Transform> LayerTransform = new Dictionary<MenuLayerOrder, Transform>();
    }

    public class MenuManager : MonoBehaviour
    {
        private static MenuManager instance;
        public static MenuManager Instance
        {
            get
            {
                if (!instance) instance = FindObjectOfType<MenuManager>();
                if (!instance)
                {
                    GameObject temp = Instantiate(Resources.Load<GameObject>("Gtion.Plugin.MenuSystem/StaticMenuSystem"));
                    instance = temp.GetComponent<MenuManager>();
                    DontDestroyOnLoad(temp);
                }

                if (instance != null && !instance.isInitialized)
                    instance.Initialize();

                return instance;
            }
        }

        [SerializeField]
        protected MenuCanvasConfig cameraConfig;
        [SerializeField]
        protected MenuCanvasConfig overlayConfig;

        //private Dictionary<MenuLayerOrder, Transform> cameraLayerTransform = new Dictionary<MenuLayerOrder, Transform>();
        //private Dictionary<MenuLayerOrder, Transform> overlayLayerTransform = new Dictionary<MenuLayerOrder, Transform>();

        private Dictionary<string, MenuData> menuDataList = new Dictionary<string, MenuData>();
        private List<BackpressAction> backpressList = new List<BackpressAction>();

        private MenuSystemConfiguration configurationDefault;
        private bool isInitialized;
        private Action BackpressActiondefault;

        #region BackpressAction Function
        /// <summary>
        /// Set current backpress action means set default action
        /// </summary>
        public Action CurrentBackpressAction
        {
            get { return backpressList.Count > 0 ? backpressList[0].callback : BackpressActiondefault; }
            set { BackpressActiondefault = value; }
        }

        private bool BackpressActionInsert(BackpressAction action)
        {
            if (action != null)
                backpressList.Insert(0, action);

            return action != null;
        }

        private void BackpressActionDelete(BackpressAction action)
        {
            if (action != null)
                backpressList.Remove(action);
        }

        #endregion

        #region Menu Function/Method

        #region Show
        public static T ShowMenu<T>(IMenuModel<T> passValue, bool isUsePool = false, bool isImmidiate = false) where T : GMenuBaseController
        {
            return ShowMenu<T>(Instance.configurationDefault.GetConfig(typeof(T).Name), isUsePool, isImmidiate, passValue);
        }

        public static T ShowMenu<T>(IMenuModel passValue = null, bool isUsePool = false, bool isImmidiate = false) where T : GMenuBaseController
        {
            return ShowMenu<T>(Instance.configurationDefault.GetConfig(typeof(T).Name), isUsePool, isImmidiate, passValue);
        }

        public static T ShowMenu<T>(MenuConfiguration config, bool isUsePool, bool isImmidiate, IMenuModel passValue) where T : GMenuBaseController
        {
            //pre-error handling
            bool isSuccess = Instance.RegisterPreErrorHandling<T>(config);
            if (!isSuccess)
            {
                Debug.LogError($"MENU_MANAGER : Failed configuration not correct");
                return null;
            }

            //Register to MenuList
            MenuData menuData = null;
            isSuccess = Instance.RegisterMenuList<T>(config, ref menuData, isUsePool);
            if (!isSuccess)
            {
                Debug.LogError($"MENU_MANAGER : Failed to Register MenuList");
                return null;
            }

            //Register to BackpressStackList
            isSuccess = Instance.RegisterBackpressStackList(config, menuData.Backpress);
            if (!isSuccess)
            {
                Debug.LogWarning($"MENU_MANAGER : Failed to Register BackpressList");
            }

            menuData.Layer = config.LayerOrder;
            menuData.Menu.transform.SetAsLastSibling();

            menuData.Menu.Init(passValue);
            if (isImmidiate)
            {
                menuData.Menu.Show();
                Debug.Log($"MENU_MANAGER : Showing Menu (Immidiate) '{typeof(T).ToString()}'");
            }
            else
            {
                Instance.StartCoroutine(Instance.ShowMenuDelayed(menuData.Menu));
            }

            return (T)menuData.Menu;
        }

        public static void RegisterDedicatedMenu<T>(T menu) where T : GMenuBaseController
        {
            bool isSuccess = Instance.RegisterBackpressStackList(menu.MenuConfig, new BackpressAction() { callback = menu.OnBackPressed });
            if (!isSuccess)
            {
                Debug.LogWarning($"MENU_MANAGER : Failed to Register BackpressList");
            }
            menu.Init(null);
        }

        private IEnumerator ShowMenuDelayed(GMenuBaseController controller)
        {
            yield return new WaitForEndOfFrame();
            Debug.Log($"MENU_MANAGER : Showing Menu '{controller.GetType().ToString()}'");
            controller.Show();
        }

        private bool RegisterPreErrorHandling<T>(MenuConfiguration config)
        {
            if (config == null)
            {
                Debug.LogError($"MENU_MANAGER : Unable to show menu '{typeof(T).ToString()}' because config is null");
                return false;
            }

            Dictionary<MenuLayerOrder, Transform> currentOrderLayer = GetLayerDictionary(config.Canvas);

            if (!currentOrderLayer.ContainsKey(config.LayerOrder))
            {
                Debug.LogError($"MENU_MANAGER : Layer '{config.LayerOrder}' not found");
                return false;
            }
            return true;
        }

        private bool RegisterMenuList<T>(MenuConfiguration config, ref MenuData menuData, bool isUsePool) where T : GMenuBaseController
        {
            Dictionary<MenuLayerOrder, Transform> currentOrderLayer = GetLayerDictionary(config.Canvas);
            if (isUsePool)
            {
                menuData = FindMenu(typeof(T), false);
                if (menuData != null && menuData.Layer != config.LayerOrder)
                {
                    menuData.Menu.transform.SetParent(currentOrderLayer[config.LayerOrder]);
                }
            }

            if (menuData == null)
            {
                GameObject menuPrefabs = config.GetGameObjectPrefab();
                if (menuPrefabs == null)
                {
                    Debug.LogError($"MENU_MANAGER : Failed to Instantiate, 'path' and 'referece' empty: '{config.ClassName}' not found");
                    return false;
                }

                GameObject tempObject = Instantiate(menuPrefabs, currentOrderLayer[config.LayerOrder]);
                tempObject.SetActive(false);

                T menuObject = tempObject.GetComponent<T>();
                if (menuObject == null)
                {
                    Destroy(tempObject); // remove to avoid memory leaking
                    Debug.LogError($"MENU_MANAGER : Failed to Get Component '{typeof(T).ToString()}' on gameobject '{config.ClassName}'");
                    return false;
                }

                menuData = GenerateNewMenuData<T>(menuObject, menuObject.OnBackPressed);
            }

            return true;
        }
        private bool RegisterBackpressStackList(MenuConfiguration config, BackpressAction backpressAction)
        {
            if (config.BlockBackPress)
            {
                return BackpressActionInsert(backpressAction);
            }
            return true;
        }

        #endregion

        #region Hide
        public void HideMenu<T>(bool saveToPool = false)
        {
            MenuData menuData = FindMenu(typeof(T));
            menuData?.Menu.Hide(saveToPool);
        }
        #endregion

        #region HideHandling
        public void HandleMenuDelete(string uniqueKey)
        {
            if (menuDataList.TryGetValue(uniqueKey, out MenuData menuData))
            {
                BackpressActionDelete(menuData.Backpress);
                menuDataList.Remove(uniqueKey);
            }
            else
            {
                Debug.LogWarning($"MENU_MANAGER : Failed to Destroy menu with uniqueKey '{uniqueKey}'");
            }
        }

        /// <summary>
        /// set active object to false instead of destroying object
        /// </summary>
        /// <param name="uniqueKey"></param>
        /// <param name="limit">limit number pool of this object, destroy if exceed, default limit is -1 equal to 20</param>
        public bool HandleMenuPool(string uniqueKey, int limit = 10)
        {
            if (menuDataList.TryGetValue(uniqueKey, out MenuData menuData))
            {
                if (IsPoolExceedLimit(menuData.TypeMenu, limit))
                {   //limit exceed
                    return false;
                }
                else
                {   //success save to pool
                    BackpressActionDelete(menuData.Backpress);
                    return true;
                }
            }
            else
            {
                Debug.LogWarning($"MENU_MANAGER : Failed to Pool menu with uniqueKey '{uniqueKey}'");
                return false;
            }
        }

        private bool IsPoolExceedLimit(Type t, int limit)
        {
            int poolCount = 0;
            foreach (KeyValuePair<string, MenuData> menuPair in menuDataList)
            {
                if (menuPair.Value.TypeMenu == t)
                    poolCount++;
            }

            return poolCount > limit;
        }
        #endregion

        #region GenerateNewMenuData
        private MenuData GenerateNewMenuData<T>(T menu, Action backpress) where T : GMenuBaseController
        {
            MenuData menuData = new MenuData();
            BackpressAction backpressAction = new BackpressAction() { callback = backpress };

            Guid g = Guid.NewGuid();
            string uniqueKey = Convert.ToBase64String(g.ToByteArray());

            menu.MenuKey = uniqueKey;

            menuData.TypeMenu = typeof(T);
            menuData.Menu = menu;
            menuData.Backpress = backpressAction;

            menuDataList.Add(uniqueKey, menuData);

            return menuData;
        }
        #endregion

        #region FindMenu
        /// <summary>
        /// Find Specific Object by reference
        /// </summary>
        /// <param name="menu"></param>
        /// <returns></returns>
        public MenuData FindMenu(string key)
        {
            MenuData result = null;
            if (menuDataList.TryGetValue(key, out result))
                return result;
            else
            {
                Debug.LogWarning($"MENU_MANAGER : Can't find menu With key '{key}'");
                return null;
            }
        }
        /// <summary>
        /// Find Specific Object by reference
        /// </summary>
        /// <param name="menu"></param>
        /// <returns></returns>
        public MenuData FindMenu(GMenuBaseController menu)
        {
            foreach (KeyValuePair<string, MenuData> menuPair in menuDataList)
            {
                MenuData menuData = menuPair.Value;
                if (menuData.Menu == menu)
                {
                    return menuData;
                }
            }
            return null;
        }

        /// <summary>
        /// Find object with Type in pool
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public MenuData FindMenu(Type type)
        {
            foreach (KeyValuePair<string, MenuData> menuPair in menuDataList)
            {
                MenuData menuData = menuPair.Value;
                if (menuData.TypeMenu == type)
                {
                    return menuData;
                }
            }
            return null;
        }

        /// <summary>
        /// Find object with Type that isActive=='isCurrentlyActive' in pool
        /// </summary>
        /// <param name="type"></param>
        /// <param name="isCurrentlyActive"></param>
        /// <returns></returns>
        public MenuData FindMenu(Type type, bool isCurrentlyActive)
        {
            foreach (KeyValuePair<string, MenuData> menuPair in menuDataList)
            {
                MenuData menuData = menuPair.Value;
                if (menuData.TypeMenu == type && menuData.IsActive == isCurrentlyActive)
                {
                    return menuData;
                }
            }
            return null;
        }
        #endregion

        #region ClearMemory
        public void ClearAll()
        {
            foreach (KeyValuePair<string, MenuData> menuPair in menuDataList)
            {
                menuPair.Value.Menu.Hide();
            }
        }
        public void ClearAll(MenuLayerOrder layer)
        {
            foreach (KeyValuePair<string, MenuData> menuPair in menuDataList)
            {
                if (menuPair.Value.Layer == layer)
                    menuPair.Value.Menu.Hide();
            }
        }
        public void ClearPool()
        {
            foreach (KeyValuePair<string, MenuData> menuPair in menuDataList)
            {
                MenuData menuData = menuPair.Value;
                if (!menuData.IsActive)
                    menuData.Menu.Hide();
            }
        }
        public void ClearPool(MenuLayerOrder layer)
        {
            foreach (KeyValuePair<string, MenuData> menuPair in menuDataList)
            {
                MenuData menuData = menuPair.Value;
                if (menuData.Layer == layer && !menuData.IsActive)
                    menuData.Menu.Hide();
            }
        }
        #endregion

        #endregion

        #region Base Function

        private void Start()
        {
            if (!isInitialized)
            {
                Initialize();
            }
        }

        public void Preload()
        {
        }

        private void Initialize()
        {
            InitConfig();
            InitLayer(overlayConfig.LayerTransform, overlayConfig.Canvas.transform);
            InitLayer(cameraConfig.LayerTransform, cameraConfig.Canvas.transform);
            InitCameraCanvas();
            isInitialized = true;

            SceneManager.sceneLoaded += InitCameraCanvas;
        }

        private void InitLayer(Dictionary<MenuLayerOrder, Transform> layerDict, Transform parent)
        {
            //creating layer gameObject 
            foreach (MenuLayerOrder enumValues in Enum.GetValues(typeof(MenuLayerOrder)))
            {
                Transform layerTransform = Instantiate(Resources.Load<GameObject>("Gtion.Plugin.MenuSystem/StaticLayer"), parent).GetComponent<Transform>();
                layerTransform.name = enumValues.ToString();
                layerDict.Add(enumValues, layerTransform);
            }
        }

        private void InitConfig()
        {
            configurationDefault = MenuSystemConfiguration.LoadConfig();
        }

        private void InitCameraCanvas(Scene scene, LoadSceneMode mode)
        {
            if (!scene.isSubScene && mode == LoadSceneMode.Single)// only change target camera when current scene is deleted
            {
                InitCameraCanvas();
            }
        }

        private void InitCameraCanvas()
        {
            Canvas cameraCanvas = cameraConfig.Canvas;
            cameraCanvas.worldCamera = Camera.main;
            cameraCanvas.sortingLayerName = "Overlay";
        }

        private Dictionary<MenuLayerOrder, Transform> GetLayerDictionary(MenuCanvasOrder canvas)
        {
            return canvas == MenuCanvasOrder.OVERLAY ? overlayConfig.LayerTransform : cameraConfig.LayerTransform;
        }

        protected void Update()
        {
            if (IsBackPressed())
            {
                CurrentBackpressAction?.Invoke();
            }
        }

        protected virtual bool IsBackPressed()
        {
            return Input.GetKeyDown(KeyCode.Escape);
        }
        #endregion

#if UNITY_EDITOR
        #region Editor Function
        [Button]
        void CreateConfig() 
        {
            MenuSystemConfiguration.CreateConfigAsset();
        }

        [Button]
        void GotoConfig()
        {
            MenuSystemConfiguration.GotoConfigAsset();
        }
        #endregion
#endif
    }
}
