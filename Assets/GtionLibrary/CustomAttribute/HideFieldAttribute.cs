
using UnityEngine;
using System.Linq;
using System;
using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;
#endif

// Created by V(Firdi) aka Firdiansyah Ramadhan

[System.AttributeUsage(System.AttributeTargets.Field)]
public class HideFieldAttribute : PropertyAttribute
{
}

#if UNITY_EDITOR
[CustomPropertyDrawer(typeof(HideFieldAttribute))]
public class HideFieldAttributeDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return -8;
    }
}
#endif