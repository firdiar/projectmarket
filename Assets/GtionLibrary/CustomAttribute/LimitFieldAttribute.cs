using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Reflection;
using System.Linq;
using System;

#if UNITY_EDITOR
using UnityEditor;
#endif

public enum LimitFieldType
{ 
    SceneOnly,
    AssetOnly
}

[System.AttributeUsage(System.AttributeTargets.Field)]
public class LimitFieldAttribute : PropertyAttribute
{
    public LimitFieldType LimitType;
    public LimitFieldAttribute(LimitFieldType limitType)
    {
        LimitType = limitType;
    }
}

#if UNITY_EDITOR
[CustomPropertyDrawer(typeof(LimitFieldAttribute))]
public class LimitFieldAttributeDrawer : PropertyDrawer
{

    // Draw the property inside the given rect
    public override void OnGUI(Rect _position, SerializedProperty _property, GUIContent _label)
    {
        LimitFieldAttribute attr = attribute as LimitFieldAttribute;

        EditorGUI.BeginProperty(_position, GUIContent.none, _property);
        _position = EditorGUI.PrefixLabel(_position, GUIUtility.GetControlID(FocusType.Passive), _label);

        if (attr.LimitType == LimitFieldType.AssetOnly)
        {
            _property.objectReferenceValue = EditorGUI.ObjectField(_position, _property.objectReferenceValue, typeof(UnityEngine.Object), false);
        }
        else //scene only
        {
            _property.objectReferenceValue = EditorGUI.ObjectField(_position, _property.objectReferenceValue, typeof(UnityEngine.Object), true);
            string path = AssetDatabase.GetAssetPath(_property.objectReferenceValue);
            if (!string.IsNullOrEmpty(path))
            {
                _property.objectReferenceValue = null;
            }
        }

        EditorGUI.EndProperty();
    }
}
#endif

