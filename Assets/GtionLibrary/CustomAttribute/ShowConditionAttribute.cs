#if UNITY_EDITOR
using System.Reflection;
using UnityEditor;
#endif
using UnityEngine;

public class ShowConditionAttribute : PropertyAttribute
{
    public string NameField { get; private set; }
    public bool TargetValue { get; private set; }
    public ShowConditionAttribute(string nameField)
    {
        this.NameField = nameField;
        TargetValue = true;
    }
    public ShowConditionAttribute(string nameField, bool targetValue)
    {
        this.NameField = nameField;
        TargetValue = targetValue;
    }
}

#if UNITY_EDITOR
/// <summary>
/// Drawer for the RequireInterface attribute.
/// </summary>
[CustomPropertyDrawer(typeof(ShowConditionAttribute))]
public class ShowConditionDrawer : PropertyDrawer
{
    bool isShowField;

    /// <summary>
    /// Overrides GUI drawing for the attribute.
    /// </summary>
    /// <param name="position">Position.</param>
    /// <param name="property">Property.</param>
    /// <param name="label">Label.</param>
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        var requiredAttribute = this.attribute as ShowConditionAttribute;
        var serializeObj = property.serializedObject;

        var propType = serializeObj.targetObject.GetType();

        var propertyRelative = serializeObj.FindProperty(requiredAttribute.NameField);
        bool isFound = propertyRelative != null;

        if (!isFound)
        {
            var propInfos = propType.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
            foreach (var info in propInfos)
            {
                if (info.Name == requiredAttribute.NameField && info.PropertyType == typeof(bool))
                {
                    isShowField = ((bool)info.GetValue(serializeObj.targetObject)) == requiredAttribute.TargetValue;
                    isFound = true;
                }
            }
        }
        else
        {
            isShowField = propertyRelative.boolValue == requiredAttribute.TargetValue;
        }


        if (isShowField || !isFound)
        {
            EditorGUI.PropertyField(position, property, label, true);
            //EditorGUILayout.PropertyField(property);
        }
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        if (isShowField)
        {
            return EditorGUI.GetPropertyHeight(property, label, true);
        }
        else
        {
            return 0;// EditorGUI.GetPropertyHeight(property, label, true) * 0.25f;
        }
    }
}
#endif