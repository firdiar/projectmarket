using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

// Created by V(Firdi) aka Firdiansyah Ramadhan
// �2022 Project SEED all right reserved
public class EnumArrayAttribute : PropertyAttribute
{
    public string[] names;
    public EnumArrayAttribute(System.Type names_enum_type)
    {
        this.names = System.Enum.GetNames(names_enum_type);
    }
}

#if UNITY_EDITOR
[CustomPropertyDrawer(typeof(EnumArrayAttribute))]
public class DrawerEnumNamedArray : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EnumArrayAttribute enumNames = attribute as EnumArrayAttribute;
        int index = System.Convert.ToInt32(property.propertyPath.Substring(property.propertyPath.IndexOf('[')).Replace("[", string.Empty).Replace("]", string.Empty));
        label.text = enumNames.names.Length > index ? enumNames.names[index] : "<NotSet>";
        //draw field
        EditorGUI.PropertyField(position, property, label, true);
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return EditorGUI.GetPropertyHeight(property, label , true);
    }
}
#endif