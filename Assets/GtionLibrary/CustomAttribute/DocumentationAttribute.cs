using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

#if UNITY_EDITOR
using UnityEditor;
#endif

// Created by V(Firdi) aka Firdiansyah Ramadhan

[AttributeUsage(AttributeTargets.Field)]
public class DocumentationAttribute : PropertyAttribute
{
    public string infoText;
    public string url;
    public bool includeDivider;
    public DocumentationAttribute(string url, bool includeDivider = true) : this("INFO : Please read the full documentation about how to use it." , url, includeDivider)
    {
    }

    public DocumentationAttribute(string infoText , string url, bool includeDivider = true)
    {
        this.infoText = infoText;
        this.url = url;
        this.includeDivider = includeDivider;
    }
}

#if UNITY_EDITOR
[CustomPropertyDrawer(typeof(DocumentationAttribute))]
public class DocumentationAttributeDrawer : PropertyDrawer
{
    DocumentationAttribute attr => (DocumentationAttribute)base.attribute;
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        Rect infoRect = new Rect(position.position, new Vector2(position.width, EditorGUIUtility.singleLineHeight+2));
        EditorGUI.HelpBox(infoRect, attr.infoText, MessageType.Info);

        infoRect.y += EditorGUIUtility.singleLineHeight+5;
        if (GUI.Button(infoRect, "Open Documentation"))
        {
            Application.OpenURL(attr.url);
        }

        if (attr.includeDivider)
        {
            infoRect.y += EditorGUIUtility.singleLineHeight + 15;
            Rect lineRect = new Rect(infoRect.position, new Vector2(position.width, 1));
            EditorGUI.DrawRect(lineRect, Color.gray);
        }
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        float normalHeight = ((EditorGUIUtility.singleLineHeight + 2) * 2) + 10;
        if (!attr.includeDivider)
        {
            return normalHeight;
        }
        else
        {
            float dividerHeight = 10;
            return normalHeight + dividerHeight;
        }
    }
}
#endif
