using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

[AttributeUsage(AttributeTargets.Field)]
public class PaddingAttribute : PropertyAttribute
{
    public int padding;
    public PaddingAttribute(int padding = 10)
    {
        this.padding = padding;
    }
}


#if UNITY_EDITOR
[CustomPropertyDrawer(typeof(PaddingAttribute))]
public class PaddingAttributeDrawer : PropertyDrawer
{
    PaddingAttribute attr => (PaddingAttribute)base.attribute;
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        position.x += attr.padding;
        position.width -= attr.padding;
        EditorGUI.PropertyField(position, property, label, true);

        Rect rect = position;
        rect.x -= 4;
        rect.width = 2;
        rect.height = GetPropertyHeight(property, label)+2;
        EditorGUI.DrawRect(rect, Color.gray);
    }
}
#endif

