using UnityEngine;
using System.Linq;
using System;
using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Reflection;

// Created by V(Firdi) aka Firdiansyah Ramadhan

[System.AttributeUsage(System.AttributeTargets.Field)]
public class FieldBtnAttribute : PropertyAttribute
{
    public enum ExistOption
    { 
        Both,
        Runtime,
        Editor
    }

    public bool isShowField;
    public readonly string[] MethodNames;
    public bool isShowFold;
    public ExistOption existOption;

    /// <summary>
    /// Adding button to inspector that execute MethodNames
    /// </summary>
    /// <param name="MethodNames">Name of method that will be invoked</param>
    public FieldBtnAttribute(params string[] MethodNames)
    {
        this.MethodNames = MethodNames;
        existOption = ExistOption.Both;
        isShowField = true;
    }

    /// <summary>
    /// Adding button to inspector that execute MethodNames
    /// </summary>
    /// <param name="existOption">Availability of Button, in Play Mode</param>
    /// <param name="MethodNames">Name of method that will be invoked</param>
    public FieldBtnAttribute(ExistOption existOption, params string[] MethodNames)
    {
        this.existOption = existOption;
        this.MethodNames = MethodNames;
        isShowField = true;
    }

    /// <summary>
    /// Adding button to inspector that execute MethodNames
    /// </summary>
    /// <param name="MethodNames">Name of method that will be invoked</param>
    public FieldBtnAttribute(bool isShowField, params string[] MethodNames)
    {
        this.MethodNames = MethodNames;
        existOption = ExistOption.Both;
        this.isShowField = isShowField;
    }

    /// <summary>
    /// Adding button to inspector that execute MethodNames
    /// </summary>
    /// <param name="existOption">Availability of Button, in Play Mode</param>
    /// <param name="MethodNames">Name of method that will be invoked</param>
    public FieldBtnAttribute(ExistOption existOption, bool isShowField, params string[] MethodNames)
    {
        this.existOption = existOption;
        this.MethodNames = MethodNames;
        this.isShowField = isShowField;
    }
}

#if UNITY_EDITOR
[CustomPropertyDrawer(typeof(FieldBtnAttribute))]
public class FieldBtnAttributeDrawer : PropertyDrawer
{
    
    private readonly float buttonHeight = EditorGUIUtility.singleLineHeight;
    private readonly float buttonSpace = 3;

    private FieldBtnAttribute attr;
    private int buttonCount;
    private float baseHeight;
    private float LabelWidth => EditorGUIUtility.labelWidth + 20;
    private float buttonTotalSpace => buttonHeight + buttonSpace;
    private int foldoutAdditional => ((attr != null && attr.MethodNames.Length > 1) ? 1 : 0);
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        attr = (FieldBtnAttribute)base.attribute;

        if (!IsAvailable())
        {
            EditorGUI.PropertyField(position, property, label, true);
            return;
        }

        buttonCount = 0;
        baseHeight = 0;
        if (attr.isShowField)
        {
            int heightMultiplier = (attr.MethodNames.Length + foldoutAdditional);
            Rect baseFieldRect = new Rect(position.x, position.y, position.width, position.height - (heightMultiplier * buttonHeight));
            EditorGUI.PropertyField(baseFieldRect, property, label, true);
            baseHeight = EditorGUI.GetPropertyHeight(property, label, true);// 
        }

        if (attr.MethodNames.Length > 1)
        {
            Rect foldoutRect = new Rect(LabelWidth, position.y + baseHeight , position.width, buttonHeight);
            attr.isShowFold = EditorGUI.Foldout(foldoutRect,attr.isShowFold, attr.isShowFold? "Collapse" : "Expand", true);
            if (attr.isShowFold)
            {
                buttonCount++;
                foreach (var name in attr.MethodNames)
                {
                    AddButton(position, property, name);
                }
                buttonCount--;
            }
        }
        else
        {
            if (attr.MethodNames.Length > 0)
            {
                AddButton(position, property, attr.MethodNames[0]);
            }
        }
    }

    private void AddButton(Rect position, SerializedProperty property, string name) 
    {
        Rect buttonRect = new Rect(LabelWidth, position.y + baseHeight + (buttonTotalSpace * buttonCount) , position.width - LabelWidth, buttonHeight);
        if (GUI.Button(buttonRect, name))
        {
            ExecuteMethod(property, name);
        }
        buttonCount++;
    }
    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        
        attr = (FieldBtnAttribute)base.attribute;
        if (IsAvailable())
        {
            return EditorGUI.GetPropertyHeight(property, label, true) + (buttonTotalSpace / 2.0f) + (buttonTotalSpace * (buttonCount + foldoutAdditional));
        }
        else
        {
            return EditorGUI.GetPropertyHeight(property, label, true);
        }
    }

    private bool IsAvailable() 
    {
        attr = (FieldBtnAttribute)base.attribute;
        if (Application.isPlaying && (attr.existOption == FieldBtnAttribute.ExistOption.Both || attr.existOption == FieldBtnAttribute.ExistOption.Runtime))
        {
            return true;
        }
        else if (!Application.isPlaying && (attr.existOption == FieldBtnAttribute.ExistOption.Both || attr.existOption == FieldBtnAttribute.ExistOption.Editor))
        {
            return true;
        }

        return false;
    }

    private object GetObjectFromList(FieldInfo fieldInfo, object instance, int index)
    {
        var listObj = fieldInfo.GetValue(instance);
        var enumerable = (IEnumerable<object>)listObj;
        return enumerable.ElementAt(index);
    }


    public object GetObjectViaPath(System.Type type, object obj, string path)
    {
        System.Type parentType = type;
        object parentObj = obj;

        System.Reflection.FieldInfo fi = type.GetField(path);
        List<string> perDot = path.Split('.').ToList();
        perDot.RemoveAt(perDot.Count - 1);

        if (perDot.Count == 0) return parentObj;

        foreach (string fieldName in perDot)
        {
            if (fieldName == "Array") continue;

            if ( parentType.IsArray || (parentType.IsGenericType && parentType.GetGenericTypeDefinition() == typeof(List<>)) )
            {
                int index = Convert.ToInt32( fieldName.Substring(fieldName.IndexOf('[')).Replace("[", string.Empty).Replace("]", string.Empty));
                parentType = parentType.GetGenericArguments()[0];
                parentObj = GetObjectFromList(fi, obj, index);
            }
            else
            {
                fi = parentType.GetField(fieldName, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
                parentObj = fi.GetValue(parentObj);

                if (fi != null)
                    parentType = fi.FieldType;
                else
                    return null;
            }


        }
        
        if (fi != null)
            return parentObj;
        else 
            return null;
    }

    private void ExecuteMethod(SerializedProperty property, string name)
    {
        UnityEngine.Object target = property.serializedObject.targetObject;
        var targetObj = GetObjectViaPath(target.GetType(), target, property.propertyPath);

        /*
        var methodInfo = targetObj
            .GetType()
            .GetMethods(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.DeclaredOnly)
            .Where(x => x.Name == name)
            .FirstOrDefault(x => x.GetParameters().Length == 0);
        */
        var methodInfo = GetMethod(targetObj, name);

        if (methodInfo != null)
        {
            methodInfo.Invoke(targetObj, null);
        }
        else
        {
            Debug.Log($"[FieldButton] Unable to find method `{name}` that have no parameters on `{target.GetType()}` prop `{property.type}` {property.propertyPath}");
        }
    }

    /// <summary>
    /// Get field using reflections
    /// </summary>
    /// <param name="target"></param>
    /// <param name="fieldName"></param>
    /// <returns></returns>
    public static MethodInfo GetMethod(object target, string fieldName)
    {
        if (target == null)
        {
            throw new ArgumentNullException("target", "The assignment target cannot be null.");
        }

        if (string.IsNullOrEmpty(fieldName))
        {
            throw new ArgumentException("fieldName", "The field name cannot be null or empty.");
        }

        Type t = target.GetType();
        MethodInfo mi = null;

        while (t != null)
        {
            var mis = t.GetMethods(BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
            mi = mis.FirstOrDefault(item => item.Name == fieldName && item.GetParameters().Length == 0);

            if (mi != null) break;

            t = t.BaseType;
        }

        if (mi == null)
        {
            throw new Exception(string.Format("Method '{0}' not found in type hierarchy.", fieldName));
        }

        return mi;
    }
}
#endif