using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Reflection;
using System.Linq;
using System;

#if UNITY_EDITOR
using UnityEditor;
#endif

[System.AttributeUsage(System.AttributeTargets.Field)]
public class DropdownListAttribute : PropertyAttribute
{
    public string MethodNames;
    public DropdownListAttribute(string MethodNames)
    {
        this.MethodNames = MethodNames;
    }
}

#if UNITY_EDITOR
[CustomPropertyDrawer(typeof(DropdownListAttribute))]
public class DropdownListAttributeDrawer : PropertyDrawer
{
    bool isShowDropdown;

    // Draw the property inside the given rect
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        DropdownListAttribute attr = attribute as DropdownListAttribute;
        string[] list = ExecuteMethod(property, attr.MethodNames);

        isShowDropdown = list != null && list.Length > 0;
        if (!isShowDropdown)
        {
            Rect baseRect = new Rect(position.position, position.size * new Vector2(1 , 0.5f));
            Rect helpRect = new Rect(position.position + new Vector2(0, EditorGUIUtility.singleLineHeight), position.size * new Vector2(1, 0.5f));
            EditorGUI.PropertyField(baseRect, property , label);
            EditorGUI.HelpBox(helpRect, "Unable to show dropdown, because list is empty", MessageType.Warning);
            return;
        }

        if (property.propertyType == SerializedPropertyType.String)
        {
            int index = Mathf.Max(0, Array.IndexOf(list, property.stringValue));
            index = EditorGUI.Popup(position, property.displayName, index, list);

            property.stringValue = list[index];
        }
        else if (property.propertyType == SerializedPropertyType.Integer)
        {
            property.intValue = EditorGUI.Popup(position, property.displayName, property.intValue, list);
        }
        else
        {
            EditorGUI.PropertyField(position, property, label);
        }
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {

        return EditorGUI.GetPropertyHeight(property, label, true) + (isShowDropdown ? 0 : EditorGUIUtility.singleLineHeight);
    }

    private string[] ExecuteMethod(SerializedProperty property, string name)
    {
        UnityEngine.Object target = property.serializedObject.targetObject;
        var methodInfo = target
            .GetType()
            .GetMethods(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.DeclaredOnly)
            .Where(x => x.Name == name)
            .FirstOrDefault(x => x.GetParameters().Length == 0);

        if (methodInfo != null)
        {
            object result = methodInfo.Invoke(target, null);
            if (result == null) return null;

            Type resultType = result.GetType();
            if (resultType.IsArray)
            {
                return (result as string[]);
            }
            else if (resultType.IsGenericType && resultType.GetGenericTypeDefinition() == typeof(List<>))
            {
                return (result as List<string>).ToArray();
            }
            else
            {
                Debug.Log($"[DropdownList] Method resulting type `{resultType}` expecting `string[]`");
            }
        }
        else
        {
            Debug.Log($"[DropdownList] Unable to find method `{name}` that have no parameters");
        }

        return null;
    }
}
#endif
