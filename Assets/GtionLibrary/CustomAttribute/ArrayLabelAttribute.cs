using UnityEngine;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;
#if UNITY_EDITOR
using UnityEditor;
#endif

// Created by V(Firdi) aka Firdiansyah Ramadhan
// �2022 Project SEED all right reserved

[AttributeUsage(AttributeTargets.Field)]
public class ArrayLabelAttribute : PropertyAttribute
{

}

[AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = false)]
public class ArrayLabelTargetAttribute : PropertyAttribute
{
}

#if UNITY_EDITOR
[CustomPropertyDrawer(typeof(ArrayLabelAttribute))]
public class DrawerPropertyLabel : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        int index = Convert.ToInt32(property.propertyPath.Substring(property.propertyPath.IndexOf('[')).Replace("[", string.Empty).Replace("]", string.Empty));
        label.text = GetLabel(property, index);

        //draw field
        EditorGUI.PropertyField(position, property, label, true);
    }

    private string GetLabel(SerializedProperty property, int index) 
    {        
        var targetObject = property.serializedObject.targetObject;
        var targetObjectClassType = targetObject.GetType();
        var field = targetObjectClassType.GetField(property.propertyPath.Split('.')[0], BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);

        if (field == null) return "Target Not Found(1)";

        var obj = GetObjectFromList(field, targetObject, index);

        if (obj == null) return "Target Not Found(2)";

        var propType = obj.GetType();
        var propInfos = propType.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
        foreach (var info in propInfos)
        {
            if (info.GetCustomAttribute<ArrayLabelTargetAttribute>() != null)
            {
                return (string)info.GetValue(obj);
            }
        }

        return "Target Not Found(3)";
    }

    private object GetObjectFromList(FieldInfo fieldInfo, object instance, int index) 
    {
        var listObj = fieldInfo.GetValue(instance);
        var enumerable = (IEnumerable<object>)listObj;
        return enumerable.ElementAt(index);
    }
    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return EditorGUI.GetPropertyHeight(property, label, true) + EditorGUIUtility.standardVerticalSpacing;// * (totalLine - 1);
    }
}
#endif