using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProductCheckoutView : MonoBehaviour
{
    [SerializeField]
    protected Text productName;
    [SerializeField]
    protected Text sellPrice;
    [SerializeField]
    protected Text totalPrice;
    [SerializeField]
    protected Text quantity;
    [SerializeField]
    protected InputField quantityField;

    [Header("Button")]
    [SerializeField]
    Button addButton;
    [SerializeField]
    Button rmvButton;
    [SerializeField]
    Button deleteButton;

    int quantityBuy;

    ProductData currentProductData;
    protected ProductData CurrentProductData => currentProductData;
    public string CurrentProductID => CurrentProductData.ID;
    public int CurrentQTY => quantityBuy;
    public int TotalPrice => CurrentQTY * CurrentProductData.SellPrice;

    public Action onUpdate;
    public Action<string> onDeleted;

    private void Start()
    {
        quantityField.onValueChanged.AddListener(OnValueChanged);
        addButton.onClick.AddListener(Add);
        rmvButton.onClick.AddListener(Remove);
        deleteButton.onClick.AddListener(DeleteItem);
    }

    public ItemDetail GetCheckoutData() 
    {
        var firestore = Firebase.Firestore.FirebaseFirestore.DefaultInstance;
        return new ItemDetail()
        {
            Product = firestore.Document(DatabaseWrapper.ProductCollectionPath + CurrentProductData.ID),
            Price = CurrentProductData.BuyPrice,
            Quantity = quantityBuy,
            FinalQuantity = Mathf.Max( 0, CurrentProductData.Quantity - quantityBuy)
        };
    }

    void DeleteItem()
    {
        Destroy(gameObject);
        onDeleted?.Invoke(CurrentProductID);
    }

    public void Initialize(ProductData data)
    {
        quantityBuy = 1;
        currentProductData = data;
        productName.text = $"{data.Name}";
        sellPrice.text = $"Rp.{ChasierMenuController.IntToCurrencyString(data.SellPrice, ".")} ,-";        
        
        UpdateTotalPrice();
    }

    public void UpdateTotalPrice()
    {
        quantityField.SetTextWithoutNotify(CurrentQTY.ToString());
        quantity.text = $"Jumlah {CurrentQTY}";
        totalPrice.text = $"Total Rp. {ChasierMenuController.IntToCurrencyString(TotalPrice, ".")}";
        onUpdate.Invoke();
    }

    public void Add()
    {
        quantityBuy++;
        UpdateTotalPrice();
    }
    void Remove()
    {
        quantityBuy = Mathf.Max(quantityBuy - 1, 0);
        UpdateTotalPrice();
    }
    void OnValueChanged(string value)
    {
        int.TryParse(value, out int result);
        quantityBuy = result;
        quantityBuy = Mathf.Max(quantityBuy, 0);
        UpdateTotalPrice();
    }

}
