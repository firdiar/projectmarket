using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProductModifyView : MonoBehaviour
{
    [SerializeField]
    protected InputField productIDInput;
    [SerializeField]
    protected InputField productNameInput;
    [SerializeField]
    protected InputField buyPriceInput;
    [SerializeField]
    protected InputField sellPriceInput;
    [SerializeField]
    protected InputField quantityInput;

    protected ProductData CurrentProductData;

    public virtual void Initialize(ProductData data) 
    {
        productIDInput.text = $"{data.ID}";
        productNameInput.text = $"{data.Name}";
        buyPriceInput.text = $"{data.BuyPrice}";
        sellPriceInput.text = $"{data.SellPrice}";
        quantityInput.text = $"{data.Quantity}";
    }

    public ProductData GetData() 
    {
        return new ProductData()
        {
            ID = productIDInput.text,
            Name = productNameInput.text,

            BuyPrice = int.Parse( buyPriceInput.text),
            SellPrice = int.Parse(sellPriceInput.text),
            Quantity = int.Parse(quantityInput.text),
        };
    }
}
