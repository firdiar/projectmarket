using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProductRegisterView : MonoBehaviour
{
    [SerializeField]
    protected Text productName;
    [SerializeField]
    protected Text buyPrice;
    [SerializeField]
    protected Text sellPrice;
    [SerializeField]
    protected Text quantity;

    [Header("Modify Input")]
    [SerializeField]
    InputField quantityAddField;
    [SerializeField]
    Button addQtyButton;
    [SerializeField]
    Button rmvQtyButton;

    [Header("Data Editor")]
    [SerializeField]
    Button deleteButton;
    [SerializeField]
    Button editDataButton;

    int quantityAdd = 0;
    ProductData currentProductData;
    protected ProductData CurrentProductData => currentProductData;
    public string CurrentProductID => CurrentProductData.ID;

    public Action<string> OnDeleted;

    private void Start()
    {
        editDataButton.onClick.AddListener(OpenEditData);
        addQtyButton.onClick.AddListener(Add);
        rmvQtyButton.onClick.AddListener(Remove);
        quantityAddField.onValueChanged.AddListener(OnValueChanged);
        deleteButton.onClick.AddListener(DeleteItem);
    }

    void DeleteItem() 
    {
        Destroy(gameObject);
        OnDeleted?.Invoke(CurrentProductID);
    }

    public void Initialize(ProductData data)
    {
        productName.text = $"{data.Name} ({data.ID})";
        buyPrice.text = $"Beli {data.BuyPrice}";
        sellPrice.text = $"Jual {data.SellPrice}";
        quantity.text = $"Jumlah {data.Quantity}";
        quantityAdd = 0;
        currentProductData = data;
    }

    public ItemDetail GetRegisterData() 
    {
        var firestore = Firebase.Firestore.FirebaseFirestore.DefaultInstance;
        return new ItemDetail()
        {
            Product = firestore.Document(DatabaseWrapper.ProductCollectionPath + CurrentProductData.ID),
            Price = CurrentProductData.BuyPrice,
            Quantity = quantityAdd,
            FinalQuantity = quantityAdd + CurrentProductData.Quantity
        };
    }

    void Add()
    {
        quantityAdd++;
        quantityAddField.SetTextWithoutNotify(quantityAdd.ToString());
        UpdateViewQTY();
    }
    void Remove()
    {
        quantityAdd = Mathf.Max(quantityAdd - 1 , 0);
        quantityAddField.SetTextWithoutNotify(quantityAdd.ToString());
        UpdateViewQTY();
    }
    void OnValueChanged(string value)
    {
        int.TryParse(value, out int result);
        quantityAdd = result;
        quantityAdd = Mathf.Max(quantityAdd, 0);
        UpdateViewQTY();
    }

    void UpdateViewQTY() 
    {
        quantity.text = $"Jumlah {CurrentProductData.Quantity} ({quantityAdd})";
    }

    void OpenEditData() 
    {
        //open some editor menu
        var model = new ModifyProductModel(CurrentProductData, (newProductData) => Initialize(newProductData));
        Gtion.Plugin.MenuSystem.MenuManager.ShowMenu(model);
    }
}
