using Gtion.Plugin.MenuSystem;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayDialogueModel : IMenuModel<DisplayDialogueController>
{
    string title;
    string content;
    string ok;
    string no;

    public string Title => title;
    public string Content => content;
    public string Ok => ok;
    public string No => no;

    Action okCallback;
    Action noCallback;
    public Action OkCallback => okCallback;
    public Action NoCallback => noCallback;

    public DisplayDialogueModel(string title, string content, Action okCallback) : this(title , content , "Ok" , "Kembali" , okCallback , null)
    {
    }

    public DisplayDialogueModel(string title, string content, string ok, string no, Action okCallback, Action noCallback) 
    {
        this.title = title;
        this.content = content;
        this.ok = ok;
        this.no = no;
        this.okCallback = okCallback;
        this.noCallback = noCallback;
    }
}

public class DisplayDialogueController : GMenuController<DisplayDialogueModel>
{
    [Header("Text")]
    [SerializeField]
    Text title;
    [SerializeField]
    Text content;

    [Header("Button")]
    [SerializeField]
    Text okText;
    [SerializeField]
    Button okButton;

    [SerializeField]
    Text noText;
    [SerializeField]
    Button noButton;

    public override void OnConstruct()
    {
        base.OnConstruct();

        okButton.onClick.AddListener(() => Model.OkCallback?.Invoke());
        noButton.onClick.AddListener(() => Model.NoCallback?.Invoke());

        okButton.onClick.AddListener(Hide);
        noButton.onClick.AddListener(Hide);
    }

    public override void Init(DisplayDialogueModel menuModel)
    {
        base.Init(menuModel);
        title.text = menuModel.Title;
        content.text = menuModel.Content;
        okText.text = menuModel.Ok;
        noText.text = menuModel.No;
    }

    public override void OnBackPressed()
    {
        Model.NoCallback?.Invoke();
        base.OnBackPressed();
    }
}
