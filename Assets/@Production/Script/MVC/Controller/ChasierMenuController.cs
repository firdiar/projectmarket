using Gtion.Plugin.MenuSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChasierModel : IMenuModel<ChasierMenuController>
{
}

public class ChasierMenuController : GMenuController<ChasierModel>
{
    [Header("View")]
    [SerializeField]
    Transform contentParentHolder;
    [SerializeField]
    ProductCheckoutView registerViewPrefab;
    [SerializeField]
    Text totalText;

    [Header("Button")]
    [SerializeField]
    Button scanButton;
    [SerializeField]
    Button cancelButton;
    [SerializeField]
    Button checkoutButton;

    List<ProductCheckoutView> listView = new List<ProductCheckoutView>();
    BarcodeScanController scanController;
    public override void OnConstruct()
    {
        base.OnConstruct();
        cancelButton.onClick.AddListener(Hide);
        checkoutButton.onClick.AddListener(TryCheckout);
        scanButton.onClick.AddListener(OpenScan);
    }

    public override void Init(ChasierModel menuModel)
    {
        base.Init(menuModel);
        RecalculateTotal();
    }
    void OpenScan()
    {
        scanController = MenuManager.ShowMenu(new BarcodeScanModel(true, ScannedItem));
    }
    async void ScannedItem(string barID)
    {
        if (string.IsNullOrEmpty(barID))
        {
            Debug.Log("Item cannot identified.");
            MenuManager.ShowMenu(new ToastModel("Item tidak dapat diidentifikasi"));
            return;
        }

        int duplicateIndex = listView.FindIndex(item => item.CurrentProductID == barID);
        bool isDuplicate = duplicateIndex != -1;
        if (isDuplicate)
        {
            listView[duplicateIndex].Add();
            MenuManager.ShowMenu(new ToastModel($"Item telah ditambahkan ({listView[duplicateIndex].CurrentQTY})"));
            return;
        }

        ProductData data = await DatabaseWrapper.LoadDetailProduct(barID);
        if (string.IsNullOrEmpty(data.ID))
        {
            data.ID = barID;
            MenuManager.ShowMenu(new ToastModel("Item belum didaftarkan"));
            MenuManager.ShowMenu(new ModifyProductModel(data, AddItem));
            scanController?.Stop();
        }
        else
        {
            AddItem(data);
        }
    }

    void AddItem(ProductData data)
    {
        MenuManager.ShowMenu(new ToastModel("Item telah ditambahkan"));
        var item = Instantiate(registerViewPrefab, contentParentHolder);
        item.onDeleted = DeleteItem;
        item.onUpdate = RecalculateTotal;
        item.Initialize(data);

        listView.Add(item);

        RecalculateTotal();
    }
    void DeleteItem(string barID)
    {
        int index = listView.FindIndex(item => item.CurrentProductID == barID);
        if (index != -1)
        {
            listView.RemoveAt(index);
        }
    }
    void RecalculateTotal() 
    {
        int total = 0;
        foreach (var item in listView)
        {
            total += item.TotalPrice;
        }

        totalText.text = "Total Rp."+ IntToCurrencyString(total, ".")+" ,-";
    }
    public void TryCheckout()
    {
        DisplayDialogueModel model = new DisplayDialogueModel("Perhatian", "Apa kamu yakin ingin checkout pesanan ini?", Checkout);
        MenuManager.ShowMenu(model);
    }

    public void Checkout() 
    {
        new DisplayDialogueModel("Perhatian", "Apa kamu yakin ingin checkout pesanan ini?", Checkout);

        Debug.Log("Checkout!");
        var registerDetail = listView.ConvertAll<ItemDetail>(item => item.GetCheckoutData());
        System.DateTime nowTime = System.DateTime.Now;
        CheckoutProduct checkoutProduct = new CheckoutProduct()
        {
            Date = nowTime,
            CheckoutedItems = registerDetail.ToArray(),
            ID = nowTime.ToString("MM-dd-yyyy HH:mm:ss")
        };
        DatabaseWrapper.SaveCheckoutProduct(checkoutProduct);

        //clear
        foreach (var obj in listView)
        {
            Destroy(obj.gameObject);
        }
        listView.Clear();
    }
    public static string IntToCurrencyString(int number, string separator)
    {
        string moneyReversed = "";

        string strNumber = number.ToString();

        int processedCount = 0;

        for (int i = (strNumber.Length - 1); i >= 0; i--)
        {
            moneyReversed += strNumber[i];

            processedCount += 1;

            if ((processedCount % 3) == 0 && processedCount < strNumber.Length)
            {
                moneyReversed += separator;
            }
        }

        string money = "";

        for (int i = (moneyReversed.Length - 1); i >= 0; i--)
        {
            money += moneyReversed[i];
        }

        return money;
    }
}
