using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Gtion.Plugin.MenuSystem;
using NaughtyAttributes;

public class BarcodeScanModel : IMenuModel<BarcodeScanController>
{
    Action<string> onScanResult;
    public Action<string> OnScanResult => onScanResult;

    bool continueAfterScan;
    public bool ContinueAfterScan => continueAfterScan;

    public BarcodeScanModel(Action<string> onScanResult) 
    {
        this.onScanResult = onScanResult;
        this.continueAfterScan = false;
    }

    public BarcodeScanModel(bool continueAfterScan, Action<string> onScanResult)
    {
        this.onScanResult = onScanResult;
        this.continueAfterScan = continueAfterScan;
    }
}

public class BarcodeScanController : GMenuController<BarcodeScanModel>
{
    [Header("Dependency")]
    [SerializeField]
    DeviceCameraController cameraController;
    [SerializeField]
    QRCodeDecodeController decodeController;

    [Header("View")]
    [SerializeField]
    GameObject scanImage;
    [SerializeField]
    Text resultText;
    [SerializeField]
    EventTrigger focusTrigger;
    [SerializeField]
    Button flipButton;
    [SerializeField]
    Button torchButton;
    [SerializeField]
    Button resetButton;

    [SerializeField]
    Button startScanButton;
    [SerializeField]
    Button stopScanButton;

    public Action<string> OnScanResult;

    bool continueAfterScanned;

    [Header("Editor Only")]
    [SerializeField]
    string scanResult;

    [Button]
    private void Editor_CapturedTest()
    {

        if (!continueAfterScanned)
        {
            decodeController.StopWork();
        }
        qrScanFinished(scanResult);
    }

    public override void Hide(bool saveToPool)
    {
        Application.targetFrameRate = 10;
        QualitySettings.vSyncCount = 4;
        base.Hide(saveToPool);
    }

    public override void Init(BarcodeScanModel model)
    {
        base.Init(model);
        Application.targetFrameRate = 60;
        QualitySettings.vSyncCount = 0;

		decodeController.onQRScanFinished.AddListener(qrScanFinished);

        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerClick;
        entry.callback.AddListener((eventData) => Focus());
        focusTrigger.triggers.Add(entry);

        startScanButton.onClick.AddListener(Play);
        stopScanButton.onClick.AddListener(Stop);

        flipButton.onClick.AddListener(cameraController.swithCamera);
        torchButton.onClick.AddListener(cameraController.toggleTorch);
        resetButton.onClick.AddListener(Reset);

        OnScanResult = model.OnScanResult;
        continueAfterScanned = model.ContinueAfterScan;

        Reset();
        Play();
    }

	public void qrScanFinished(string dataText)
	{
		Debug.Log("GetResult : "+dataText);

        resultText.text = dataText;
        resetButton.gameObject.SetActive(true);
        scanImage.SetActive(false);

        OnScanResult?.Invoke(dataText);

        if (continueAfterScanned)
        {
            Play();
        }
    }

    private void Focus()
    {
        Debug.Log("Foxus");
        cameraController.tapFocus();
    }

    public void Play()
    {
        Reset();        
        decodeController.StartWork();
        Focus();
    }

    public void Stop()
    {
        decodeController.StopWork();

        resetButton.gameObject.SetActive(false);
        scanImage.SetActive(false);

        Hide();
    }

    private void Reset()
    {
        decodeController.Reset();

        resultText.text = string.Empty;
        resetButton.gameObject.SetActive(false);
        scanImage.SetActive(true);
    }
}
