using Gtion.Plugin.MenuSystem;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ModifyProductModel : IMenuModel<ModifyProductController>
{
    ProductData productData;
    public ProductData ProductData => productData;

    Action<ProductData> onFinish;
    public Action<ProductData> OnFinish => onFinish;

    public ModifyProductModel(ProductData data)
    {
        productData = data;
    }

    public ModifyProductModel(ProductData data, Action<ProductData> onFinish) : this(data)
    {
        this.onFinish = onFinish;
    }
}
public class ModifyProductController : GMenuController<ModifyProductModel>
{
    [SerializeField]
    ProductModifyView productModifyView;

    [SerializeField]
    Button saveBtn;
    [SerializeField]
    Button cancelBtn;

    Action<ProductData> onFinish;
    public override void Init(ModifyProductModel menuModel)
    {
        base.Init(menuModel);
        productModifyView.Initialize(menuModel.ProductData);
        onFinish = menuModel.OnFinish;
    }

    public override void OnConstruct()
    {
        base.OnConstruct();
        cancelBtn.onClick.AddListener(Hide);
        saveBtn.onClick.AddListener(SaveProductData);
    }

    void SaveProductData() 
    {
        ProductData productData = productModifyView.GetData();
        DatabaseWrapper.SaveDetailProduct(productData);
        onFinish?.Invoke(productData);
        Hide();
    }
}
