using Gtion.Plugin.MenuSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class RegisterProductModel : IMenuModel<RegisterProductMenuController> 
{
}

public class RegisterProductMenuController : GMenuController<RegisterProductModel>
{
    [Header("View")]
    [SerializeField]
    Transform contentParentHolder;
    [SerializeField]
    ProductRegisterView registerViewPrefab;

    [Header("Button")]
    [SerializeField]
    Button scanButton;
    [SerializeField]
    Button cancelButton;
    [SerializeField]
    Button registerButton;

    List<ProductRegisterView> listView = new List<ProductRegisterView>();
    BarcodeScanController scanController;
    public override void OnConstruct()
    {
        base.OnConstruct();
        cancelButton.onClick.AddListener(Hide);
        registerButton.onClick.AddListener(TrySaveRecord);
        scanButton.onClick.AddListener(OpenScan);
    }

    void OpenScan() 
    {
        scanController = MenuManager.ShowMenu(new BarcodeScanModel(true, ScannedItem));
    }

    async void ScannedItem(string barID) 
    {
        if (string.IsNullOrEmpty(barID))
        {
            Debug.Log("Item cannot identified.");
            MenuManager.ShowMenu(new ToastModel("Item tidak dapat diidentifikasi"));
            return;
        }

        bool isDuplicate = listView.FindIndex(item => item.CurrentProductID == barID) != -1;
        if (isDuplicate)
        {
            Debug.Log("Item already exist in list.");
            MenuManager.ShowMenu(new ToastModel("Item telah ditambahkan"));
            return;
        }

        ProductData data = await DatabaseWrapper.LoadDetailProduct(barID);
        if (string.IsNullOrEmpty(data.ID))
        {
            data.ID = barID;
            MenuManager.ShowMenu(new ToastModel("Item belum didaftarkan"));
            MenuManager.ShowMenu(new ModifyProductModel(data, AddItem));
            scanController?.Stop();
        }
        else
        {
            AddItem(data);
        }
    }

    void AddItem(ProductData data)
    {
        MenuManager.ShowMenu(new ToastModel("Item telah ditambahkan"));
        var item = Instantiate(registerViewPrefab, contentParentHolder);
        item.Initialize(data);
        item.OnDeleted = DeleteItem;
        listView.Add(item);
    }

    void DeleteItem(string barID)
    {
        int index = listView.FindIndex(item => item.CurrentProductID == barID);
        if(index != -1)
        {
            listView.RemoveAt(index);
        }
    }

    public void TrySaveRecord()
    {
        DisplayDialogueModel model = new DisplayDialogueModel("Perhatian", "Apa kamu yakin ingin menyimpan ke database produk ini?", SaveRecord);
        MenuManager.ShowMenu(model);
    }

    void SaveRecord() 
    {
        var registerDetail = listView.ConvertAll<ItemDetail>(item => item.GetRegisterData());
        System.DateTime nowTime = System.DateTime.Now;
        RegisterProduct registerProductData = new RegisterProduct()
        {
            Date = nowTime,
            RegisteredItem = registerDetail.ToArray(),
            ID = nowTime.ToString("MM-dd-yyyy HH:mm:ss")
        };
        DatabaseWrapper.SaveRegisterProduct(registerProductData);

        //clear
        foreach (var obj in listView)
        {
            Destroy(obj.gameObject);
        }
        listView.Clear();
    }
}
