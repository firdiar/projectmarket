using Gtion.Plugin.MenuSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AdminModel : IMenuModel<AdminMenuController> { }

public class AdminMenuController : GMenuController<AdminModel>
{
    [SerializeField]
    Button registerBtn;
    [SerializeField]
    Button backButton;

    public override void OnConstruct()
    {
        base.OnConstruct();
        backButton.onClick.AddListener(Hide);
        registerBtn.onClick.AddListener(OpenRegisterProduct);
    }

    public void OpenRegisterProduct() 
    {
        MenuManager.ShowMenu(new RegisterProductModel());
    }

}
