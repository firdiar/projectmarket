using Gtion.Plugin.MenuSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuModel : IMenuModel<MainMenuController> { }

public class MainMenuController : GMenuController<MainMenuModel>
{
    [SerializeField]
    Button cashierButton;
    [SerializeField]
    Button adminButton;
    [SerializeField]
    Button backButton;

    public override void OnConstruct()
    {
        base.OnConstruct();
        cashierButton.onClick.AddListener(OpenCashierPanel);
        adminButton.onClick.AddListener(OpenAdminPanel);
        backButton.onClick.AddListener(Hide);
    }

    void OpenCashierPanel()
    {
        MenuManager.ShowMenu(new ChasierModel());
    }
    void OpenAdminPanel() 
    {
        MenuManager.ShowMenu(new AdminModel());
    }

    public override void Hide(bool saveToPool)
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }
}
