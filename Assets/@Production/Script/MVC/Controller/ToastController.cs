using Gtion.Plugin.MenuSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToastModel : IMenuModel<ToastController> 
{
    string text;
    public string Text => text;
    public ToastModel(string text)
    {
        this.text = text;
    }
}
public class ToastController : GMenuController<ToastModel>
{
    [SerializeField]
    Text toastText;
    [SerializeField]
    Text toastTextPrediction;

    public override void Init(ToastModel menuModel)
    {
        base.Init(menuModel);
        toastText.text = menuModel.Text;
        toastTextPrediction.text = menuModel.Text;
    }
}
