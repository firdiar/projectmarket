using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase.Firestore;
using System.Threading.Tasks;

public static class DatabaseWrapper
{
    const string ProductCollectionKey = "Product";
    public const string ProductCollectionPath = ProductCollectionKey+"/";

    const string RegisterCollectionKey = "RegisterRecord";
    public const string RegisterCollectionPath = RegisterCollectionKey + "/";


    const string CheckoutCollectionKey = "CheckoutRecord";
    public const string CheckoutCollectionPath = CheckoutCollectionKey + "/";

    #region Product Detail
    public static async Task<ProductData> LoadDetailProduct(string ID)
    {
        var firestore = FirebaseFirestore.DefaultInstance;
        var result = await firestore.Document(ProductCollectionPath + ID).GetSnapshotAsync();
        if (result.Exists)
        {
            ProductData data = result.ConvertTo<ProductData>();
            return data;
        }
        else
        {
            return new ProductData();
        }
    }

    public static async Task<List<ProductData>> LoadDetailProduct(params string[] IDs)
    {
        var firestore = FirebaseFirestore.DefaultInstance;
        var collection = firestore.Collection(ProductCollectionKey);

        Task<DocumentSnapshot>[] batchRead = new Task<DocumentSnapshot>[IDs.Length];
        for(int i = 0; i <  IDs.Length; i++)
        {
            var task = collection.Document(IDs[i]).GetSnapshotAsync();
            batchRead[i] = task;
        }
        DocumentSnapshot[] documents = await Task.WhenAll(batchRead); ;

        List <ProductData> dataList = new List<ProductData>();
        foreach (var doc in documents)
        {
            if (doc.Exists)
            {
                ProductData data = doc.ConvertTo<ProductData>();
                dataList.Add(data);
            }
        }

        return dataList;
    }


    public static async void SaveDetailProduct(ProductData data)
    {
        var firestore = FirebaseFirestore.DefaultInstance;

        await firestore.Document(ProductCollectionPath + data.ID).SetAsync(data);
        Debug.Log("Data Saved!");
    }

    public static async void SaveDetailProduct(List<ProductData> datas)
    {
        var firestore = FirebaseFirestore.DefaultInstance;
        WriteBatch batch = firestore.StartBatch();

        foreach (var data in datas)
        {
            var reference = firestore.Document(ProductCollectionPath + data.ID);
            batch.Set(reference , data);
        }
        
        await batch.CommitAsync();
        Debug.Log("Data Saved!");
    }
    #endregion

    #region Register Product
    public static async void SaveRegisterProduct(RegisterProduct data)
    {
        var firestore = FirebaseFirestore.DefaultInstance;
        WriteBatch batch = firestore.StartBatch();
        batch.Set(firestore.Document(RegisterCollectionPath + data.ID), data);
        foreach (var detail in data.RegisteredItem)
        {
            batch.Update(detail.Product, nameof(ProductData.Quantity), Mathf.Max( 0 , detail.FinalQuantity));
        }
        await batch.CommitAsync();
        Debug.Log("Data Saved!");
    }

    public static async void LoadRegisterProduct()
    {
        var firestore = FirebaseFirestore.DefaultInstance;
        var collection = firestore.Collection(RegisterCollectionKey);
        var query = await collection.GetSnapshotAsync();
        foreach (var doc in query.Documents)
        {
            Debug.Log(doc);
            Debug.Log(doc.Id);
            RegisterProduct reg = doc.ConvertTo<RegisterProduct>();
        }
        Debug.Log("Data Loaded!");
    }
    #endregion

    #region Register Product
    public static async void SaveCheckoutProduct(CheckoutProduct data)
    {
        var firestore = FirebaseFirestore.DefaultInstance;
        WriteBatch batch = firestore.StartBatch();
        batch.Set(firestore.Document(CheckoutCollectionPath + data.ID), data);
        foreach (var detail in data.CheckoutedItems)
        {
            batch.Update(detail.Product, nameof(ProductData.Quantity), Mathf.Max(0, detail.FinalQuantity));
        }
        await batch.CommitAsync();
        Debug.Log("Data Saved!");
    }
    #endregion
}
