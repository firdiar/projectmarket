using Gtion.Plugin.MenuSystem;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class MainProgram : MonoBehaviour
{
    async void Start()
    {
        Application.targetFrameRate = 10;
        QualitySettings.vSyncCount = 4;

        MenuManager.ShowMenu(new MainMenuModel());
        var result = await Firebase.FirebaseApp.CheckAndFixDependenciesAsync();
        if (result == Firebase.DependencyStatus.Available)
        {
            Debug.Log("Firebase Ready!");
        }
        else
        {
            Debug.Log("Firebase Missing!");
        }
    }
}
