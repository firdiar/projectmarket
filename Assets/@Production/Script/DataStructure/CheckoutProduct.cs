using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Firebase.Firestore;

[FirestoreData]
public struct CheckoutProduct
{
    [FirestoreDocumentId]
    public string ID { get; set; }

    [FirestoreProperty]
    public DateTime Date { get; set; }

    [FirestoreProperty]
    public ItemDetail[] CheckoutedItems { get; set; }
}
