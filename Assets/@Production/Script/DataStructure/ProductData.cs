using Firebase.Firestore;

[FirestoreData]
public struct ProductData
{
    [FirestoreDocumentId]
    public string ID { get; set; }

    [FirestoreProperty]
    public string Name { get; set; }

    [FirestoreProperty]
    public int BuyPrice { get; set; }

    [FirestoreProperty]
    public int SellPrice { get; set; }

    [FirestoreProperty]
    public int Quantity { get; set; }

    public override string ToString()
    {
        return $"{ID} - {Name} - {Quantity}";
    }
}
