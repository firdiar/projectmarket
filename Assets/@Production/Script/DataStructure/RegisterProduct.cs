using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Firebase.Firestore;

[FirestoreData]
public struct RegisterProduct
{
    [FirestoreDocumentId]
    public string ID { get; set; }

    [FirestoreProperty]
    public DateTime Date { get; set; }

    [FirestoreProperty]
    public ItemDetail[] RegisteredItem { get; set; }
}

[FirestoreData]
public struct ItemDetail
{
    [FirestoreProperty]
    public DocumentReference Product { get; set; }

    [FirestoreProperty]
    public int Price { get; set; }

    [FirestoreProperty]
    public int Quantity { get; set; }

    /// <summary>
    /// Quantity after being decreased or increased
    /// </summary>
    public int FinalQuantity { get; set; }
}
