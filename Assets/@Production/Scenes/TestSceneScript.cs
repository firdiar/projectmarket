using Gtion.Plugin.MenuSystem;
using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestSceneScript : MonoBehaviour
{
    // Start is called before the first frame update
    async void Start()
    {
        MenuManager.ShowMenu(new MainMenuModel());
        var result = await Firebase.FirebaseApp.CheckAndFixDependenciesAsync();
        if (result == Firebase.DependencyStatus.Available)
        {
            Debug.Log("Firebase Ready!");
        }
        else
        {
            Debug.Log("Firebase Missing!");
        }
    }

    [Button]
    void OpenMainMenu()
    {
        MenuManager.ShowMenu(new MainMenuModel());
    }

    [Button]
    void OpenScanner()
    {
        MenuManager.ShowMenu(new BarcodeScanModel(null));
    }

    [Button]
    async void LoadDatabase()
    {
        var firestore = Firebase.Firestore.FirebaseFirestore.DefaultInstance;
        var result = await firestore.Document("Product/000111").GetSnapshotAsync();
        ProductData data = result.ConvertTo<ProductData>();
        Debug.Log(data.ToString());
    }

    [Button]
    async void LoadDatabaseBatch()
    {
        List<ProductData> productDatas = await DatabaseWrapper.LoadDetailProduct( "000111", "123123");
        foreach (var data in productDatas)
        {
            Debug.Log(data.ToString());
        }
    }

    [Button]
    async void SaveDatabaseRegister()
    {
        RegisterProduct registerProductData = new RegisterProduct()
        {
            ID = "123123",
            Date = System.DateTime.Now,
            RegisteredItem = new ItemDetail[1] { new ItemDetail() { Quantity = 1 , Price = 100} }
        };
        DatabaseWrapper.SaveRegisterProduct(registerProductData);
    }
    [Button]
    async void LoadDatabaseRegister()
    {
        DatabaseWrapper.LoadRegisterProduct();
    }

    [Button]
    async void SaveDatabase()
    {
        ProductData data = new ProductData()
        {
            Name = "Pep",
            BuyPrice = 100,
            SellPrice = 110,
        };
        var firestore = Firebase.Firestore.FirebaseFirestore.DefaultInstance;
        await firestore.Document("Product/000111").SetAsync(data);

        Debug.Log("Update Finish!");
    }
}
